// Solves the problem given here https://paradox.kattis.com/problems/paradoxpath
#include <unordered_map>
#include <queue>
#include <functional>

typedef std::pair<int, int> Cell;

int ReconstructPath(const int nStart, const int nTarget, const int nOutBufferSize, int* pOutBuffer,
					std::unordered_map<int, int>& mCameFrom);

Cell toCoord(const int nNode, const int nMapWidth);
int toNode(const int nX, const int nY, const int nMapWidth);
int HeuristicManhattanEstimate(const int nFromX, const int nFromY, const int nToX, const int nToY);
int ReconstructPath(const int nStart, const int nTarget,
					const int nOutBufferSize, int* pOutBuffer,
					std::unordered_map<int, int>& mCameFrom);



int FindPath(const int nStartX, const int nStartY, const int nTargetX, const int nTargetY,
			 const unsigned char* pMap, const int nMapWidth, const int nMapHeight, int* pOutBuffer, const int nOutBufferSize) {

	if (nStartX == nTargetX && nStartY == nTargetY){
		return 0;
	}

	std::unordered_map<int, int> score;
	std::unordered_map<int, int> cameFrom; 
	std::priority_queue<Cell, std::vector<Cell>, std::greater<Cell>> openSet;

	int start = toNode(nStartX, nStartY, nMapWidth);
	int goal = toNode(nTargetX, nTargetY, nMapWidth);

	openSet.emplace(0, start);
	score[start] = 0;

	const int possibleAxisMovement[8] = { -1, 0, 1, 0, 0, -1, 0, 1 }; 
	while (!openSet.empty()) {
		int curr = openSet.top().second;
		openSet.pop();

		Cell currNode = toCoord(curr, nMapWidth);
		if (curr == goal) {
			return ReconstructPath(start, goal, nOutBufferSize, pOutBuffer,
				cameFrom);
		}

		// Go over all neighbours in current cell
		for (int i = 0; i < 8; i += 2) {
			int nextX = currNode.first + possibleAxisMovement[i];
			int nextY = currNode.second + possibleAxisMovement[i + 1];
			int nextNeighbour = toNode(nextX, nextY, nMapWidth);

			// Pick only valid, clear cell
			if (cameFrom.find(nextNeighbour) != cameFrom.end())
				continue;

			if (nextX >= 0 && (nextX < nMapWidth) && (nextY >= 0) && (nextY < nMapHeight) && pMap[nextNeighbour]) {
				int newScore = score[curr] + 1;
				if (!score.count(nextNeighbour) || newScore < score[nextNeighbour]) {
					int priority = newScore + HeuristicManhattanEstimate(nextX, nextY, nTargetX, nTargetY);

					cameFrom[nextNeighbour] = curr;
					score[nextNeighbour] = newScore;
					openSet.emplace(priority, nextNeighbour);
				}
			}
		}
	}

	return -1;
}

// Raising this allows for faster runtime in exchange for potentially longer paths
const int d = 10;
int HeuristicManhattanEstimate(const int currX, const int currY, const int destX, const int destY) {
	return d * (std::abs(currX - destX) + std::abs(currY - destY));
}

// Convert from row-major to x and y
Cell toCoord(const int node, const int mapWidth) {
	return Cell(node % mapWidth, node / mapWidth);
}

// Covert from x and y to row major ordering
int toNode(const int x, const int y, const int mapWidth) {
	return y * mapWidth + x;
}

int ReconstructPath(const int start, const int target, const int nOutBufferSize, int* pOutBuffer,
					std::unordered_map<int, int>& cameFrom) {
	std::vector<int> path;
	int curr = target;
	path.push_back(curr);

	while (curr != start) {
		curr = cameFrom[curr];
		path.push_back(curr);
	}

	int j = 0;
	for (auto it = path.end() - 1; it != path.begin() && j < nOutBufferSize; --it)
	{
		pOutBuffer[j++] = *it;
	}

	return path.size() - 1;
}