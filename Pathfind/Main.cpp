#include "Path.h"
#include <iostream>

int main(){

	const int bufSize = 100;
	const unsigned char map[] = {
		1, 1, 0, 1, 1, 1,
		1, 1, 1, 1, 1, 1,
		1, 0, 1, 0, 1, 0,
		1, 0, 1, 1, 0, 1,
		1, 1, 1, 1, 1, 1,
		0, 0, 0, 0, 0, 1
	};
	int outBuf[bufSize];

	std::cout << FindPath(0, 0, 5, 5, map, 6, 6, outBuf, bufSize);

	return 0;
}
